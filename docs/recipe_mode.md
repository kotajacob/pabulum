Recipe Mode
============

To use the command in recipe mode use this syntax.

pub recipe [operation] [recipe name or id]

### Operation

The following operations can be used to work with the recipes.  

-a = Add operation  

-p = Print operation  
default operation

-s = Set operation  

-r = Remove operation  

### Examples

Print the "homemade pizza" recipe  
`pub recipe -p "homemade pizza"`

Add the "homemade pizza" recipe  
`pub recipe -p "homemade pizza:my pizza recipe","A homemade pizza recipe","Dinner","10.99","1200","mozzerella:pizza sauce:pizza dough","2","15"`

Remove the "homemade pizza" recipe  
`pub recipe -r "homemade pizza"`

