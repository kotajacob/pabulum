Recipe and Ingredient Elements
===============================

### Ingredients

0. ID
1. Name
2. Description
3. Type
4. Price
5. Calories
6. Expires

### Recipes

0. ID
1. Name
2. Description
3. Type
4. Price
5. Calories
6. Ingredients
7. Prep Time
8. Cook Time

ID - A single ID used to quickly identify the recipe or ingredient internally.

Name - The name of the ingredient or recipe.

Description - (optional) A brief description of the ingredient or recipe.

Type - (optional) A listing of any categories the ingredient or recipe falls under.

Price - (optional) The price of the ingredient or recipe.

Calories - (optional) The calorie count of an ingredient or recipe.

Expires - (optional) The date the ingredient will expire.

Ingredients - A listing of the ingredients used in a recipe.

Prep Time - (optional) The time it takes to prep the recipe.

Cook Time - (optional) The time it takes to cook the recipe.
