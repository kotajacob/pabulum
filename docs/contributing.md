Contributing Guide
===================

Currently the project is in its infancy and needs lots of planning and 
documentation work. If you'd like to help below is a list of some things 
you could do the contribute to pabulum.

* Read through the docs and create an issue for any spelling, grammer, or wording mistakes. (tagged as bugs)
* Make new feature suggestions by creating an issue. (tagged as suggestion)
* Check the issues and see if there are any unassigned issues you could fix. Then make a pull request with the fix.
