Storage Mode
=============

To use the command in storage mode use this syntax.

pub store [operation] [option] [content]

### Operation

The following operations can be used to work with the storage mode.

-a = Add operation  

-p = Print operation  
default operation

-r = Remove operation  

### Option (only used with print mode)

-e = Sort by expired  

-E = Show expired only  

-f = Sort by freshness  
default option

-F = Show fresh only  

-m = Sort by cost low to high  

-M = Sort by cost high to low  

-c = Sort by calories low to high  

-C = Sort by calories high to low  

-t = Show only one type of ingredient  

### Examples

Print all ingredients in storage.  
`pub store -p`

Add 3 apples to storage.  
`pub store -a "apple" 3`

Print all expired ingredients in storage.  
`pub store -pE`
