Database Specs
===============

Format Comma-separated values (.csv)

### Ingredients Database

0. int Id (ingredients)
1. list Name
2. string Description
3. list Type (seperated by : )
4. double Price
5. double Calories
6. int Expires (unix time at the start of day)

### Recipe Database

0. int Id (recipes)
1. list Name
2. string Description
3. list Type (seperated by : )
4. double Price
5. double Calories
6. list Ingredients (sepereated by : )
7. int Prep Time (in seconds)
8. int Cook Time (in seconds)

### Planner Database

1. int Date (unix time at the start of day)
2. list Recipe IDs (seperated by : )

### Storage Database

0. int Id (ingredients)
1. list Name
2. int Amount
3. int Date (unix time at the start of day) 

### Log Database

1. int Id (either, use type to check) 
2. list Name
3. int Type (planned = 0/quick meal = 1/manual addition = 2/expired = 3/4,5,6 same thing but for ingredients)
4. int Date (unix time at the start of day)
