Planner Mode
=============

To use the command in planner mode use this syntax.

pub plan [operation] [time] [generator settings]

### Operation

The following operations can be used to work with the planner.

-a = Add operation

-g = Generate operation

-p = Print operation  
default operation

-s = Set operation

-r = Remove operation

-i = Print ingredients list (groceries) operation

### Time

The following time options can be used to specify a time.

-D # = Which day (date number) to use.  
default: current day  
default time option

-d # = Which day (starting from the current day) to use.  
default: current day

-W # = Which week (week number) to use.  
default: current week

-w # = Which week (starting with 0 as the current week) to use.  
default: current week

-M # = Which month (month number) to use.  
default: current month

-m # = Which month (starting with 0 as the current month) to use.  
default: current month

-Y # = Which year (year number) to use.  
default: current year

-y # = Which year (starting with 0 as the current year) to use.  
default: current year

### Generator Settings

-n = Number of meals per day.  
default 3

-c = Provide a range of calories per meal.  

-C = Provide a range of calories per day.  
default "1800:2400"

-m = Provide a range of cost per meal.

-M = Provide a range of cost per day.

### Examples

Print list of ingredients for the week.  
`pub plan -iW`

Print the current week's plan.   
`pub plan -pW`

Print the current month's plan.  
`pub plan -pM`

Print next week's plan.  
`pub plan -pw 1`

Print today's plan.  
`pub plan -pD`

print tomorrow's plan.  
`pub plan -pd 1`

Print july 16th's plan.  
`pub plan -pd "16 6"`

Add a meal to today.  
`pub plan -aD "recipe name"`

Set 3 meals for tomorrow.  
`pub plan -sd 1 "recipe 1" "recipe 2" "recipe 3"`
