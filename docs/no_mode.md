No Mode
========

Using the command with no mode can be used to find helpful information about pabulum.

pub [operation]

### Operation

The following operations can be used without a mode.

-v --version = Print the current version of pabulum.

-h --help = Print a help message about pabulum.

-u --update = Attempt to update the database from out repo online.
