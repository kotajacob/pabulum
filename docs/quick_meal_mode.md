Quick Meal Mode
================

To use the command in quick meal mode use this syntax.

pub meal [operation] [setting]

### Operation

The following operations can be used to work with quick meal.

-p = Print operation  
default operation

-a = Use all available recipes (not just ones you have ingredients for)

### Setting

-c = Show low calorie recipes first  

-C = Show high calorie recipes first  

-m = Show low cost recipes first  

-M = Show high cost recipes first  

-t = Show low time recipes first  
default setting

-T = Show high time recipes first  

### Examples  

Print quick recipes.  
`pub meal`

Print a listing of low calorie meals using all known recipes.  
`pub meal -ac`
