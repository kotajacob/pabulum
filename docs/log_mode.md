Log Mode
=========

To use the command in log mode use this syntax.

pub log [operation] [time] [type]

### Operation

The following operations can be used to work with the log.

-w = Add operation

-p = Print operation  
default operation  

-s = Set operation

-r = Remove operation

### Time

The following time options can be used to specify a time.

-D # = Which day (date number) to use.  
default: current day  
default time option  

-d # = Which day (starting from the current day going backwards) to use.  
default: current day

-W # = Which week (week number) to use.  
default: current week

-w # = Which week (starting with 0 as the current week going backwards) to use.  
default: current week

-M # = Which month (month number) to use.  
default: current month

-m # = Which month (starting with 0 as the current month going backwards) to use.  
default: current month

-Y # = Which year (year number) to use.  
default: current year

-y # = Which year (starting with 0 as the current year going backwards) to use.  
default: current year

### Type

-a = Using all.  
default option

-c = Using calories.  

-m = Using money.  

### Examples

Print this week's log.  
`pub log -pW`

Print last week's log.  
`pub log -pw 1`

Add a recipe to today.  
`pub log -aD "recipe name"`

Set yesterdays recipes.  
`pub log -sd 1 "recipe 1" "recipe 2" "recipe 3"`
