Pabulum Modes
==============

* Planner
* Quick Meal
* Storage
* Recipes
* Log

### Planner (pub plan)

The planner mode is the bulk of pabulum and serves as the main goal. The planner 
mode is used to store a calendar with each meal planned out for each day. Going 
as far forward as the user would like. This meal plan can be either set 
manually or generated with a whole slew of options. When using the planner 
mode you must specify a mode and a time or time period.

### Quick Meal (pub meal)

Pabulum can be used to serve another distinct purpose. It can keep track of all 
items in your refrigerator and at any time via the quick meal mode list all the 
recipes (from your recipe database) that could be made with your ingredients.

### Storage (pub store)

The storage mode is the part of pabulum that keeps track of all the food 
ingredients you currently have in stock. It can be used to tell you what you're 
low on and what you use most often. Also it works with the quick meal mode 
allowing you to easily see what recipes you could make at any time.

### Recipes (pub recipe)

This mode is used to create and add new recipes or ingredients to your database.
It can even be used to update or import other databases you may find online. 
This is very useful for building your cookbook and having lots of recipes to 
pick from when using your planner or selecting a quick meal.

### Log (pub log)

The last mode of pabulum is a log of all the things you've used pabulum for. It's 
easily searchable to find out how much you've been spending on food, or how many 
calories you've been eating, or even just what kinds of things tend to be your 
favorite meals and ingredients.
