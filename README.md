Pabulum
========

We are currently in development of our first version. Check back later for 
our first release.

Pabulum is an all in one food management system. You can use it as a meal 
planner, calorie tracker, food money tracker, food stock inventory, recipe 
manager, or even to generate quick meals from the ingredients you have in 
stock.

Pabulum is open source and the goal is to first to make a simple command 
line version called pub and then to expand to a desktop and mobile client.

Currently the program will be used in a few different modes. 

* Planner
* Quick Meal
* Storage
* Recipes
* Log

For information about each of these modes check the corresponding mode.md file 
here in the documents folder.

If you'd like to contribute code, suggestions, or any other way check the 
contributing.md file to learn more about making a good contribution.

Copyright (C) 2017  Dakota Walsh
